 
# Importing required packages
import pandas as pd
import ixmp as ix
import message_ix

from message_ix.util import make_df



mp = ix.Platform(
    backend="jdbc",
    driver="hsqldb",
    url="jdbc:hsqldb:mem:temporary platform",
)


# Specifying model/scenario to be loaded from the database
model = 'Westeros Electrified'
scenario='baseline'
base = message_ix.Scenario(mp, model, scenario)

# Cloning a scenario for adding time steps
scen = base.clone(model, 'westeros_seasonal', 'introducing seasonality', keep_solution=False)
scen.check_out()


# Adding sub-annual time steps
time_steps = ['winter', 'summer']
scen.add_set('time', time_steps)


# We can see the elements of the set
scen.set('time')


# Defining a new temporal level
time_level = 'season'
scen.add_set('lvl_temporal', time_level)


# Adding temporal hierarchy
for t in time_steps:
    scen.add_set('map_temporal_hierarchy', [time_level, t, 'year'])
    
# We can see the content of the set
scen.set('map_temporal_hierarchy')

# All parameters with at least one sub-annual time index
parameters = [p for p in scen.par_list() if 'time' in scen.idx_sets(p)]

# Those parameters with "time" index that are not empty in our model
[p for p in parameters if not scen.par(p).empty]




# Adding duration time
for t in time_steps:
    scen.add_par('duration_time', [t], 0.5, '-')



def yearly_to_season(scen, parameter, data, filters=None):
    if filters:
        old = scen.par(parameter, filters)
    else:
        old = scen.par(parameter)
    scen.remove_par(parameter, old)
    
    # Finding "time" related indexes
    time_idx = [x for x in scen.idx_names(parameter) if 'time' in x]
    for h in data.keys():
        new = old.copy()
        for time in time_idx:
            new[time] = h
        new['value'] = data[h] * old['value']
        scen.add_par(parameter, new)




scen.par('demand')

# Modifying demand for each season
demand_data = {'winter': 0.60, 'summer': 0.40}
yearly_to_season(scen, 'demand', demand_data)

# let's look at "demand" now
scen.par('demand')


# Modifying input and output parameters for each season
# output
fixed_data = {'winter': 1, 'summer': 1}
yearly_to_season(scen, 'output', fixed_data)

# input
yearly_to_season(scen, 'input', fixed_data)


# Modifying growth rates for each season
yearly_to_season(scen, 'growth_activity_up', fixed_data)



# Modifying capacity factor
# Let's get the yearly capacity factor of wind in the baseline scenario
cf_wind = scen.par('capacity_factor', {'technology': 'wind_ppl'})['value'].mean()

# Converting yearly capacity factor to seasonal
cf_data = {'winter': 0.46 / cf_wind, 'summer': 0.25 / cf_wind} 
cf_filters = {'technology': 'wind_ppl'}
yearly_to_season(scen, 'capacity_factor', cf_data, cf_filters)

# Capacity factor of other technologies remains unchanged in each season
cf_filters = {'technology': ['coal_ppl', 'bulb', 'grid']}
yearly_to_season(scen, 'capacity_factor', fixed_data, cf_filters)

# Let's look at capacity factor in year 710
scen.par('capacity_factor', {'year_act':710, 'year_vtg':710})


# Modifying historical activity 
hist_data = {'winter': 0.5, 'summer': 0.5}
yearly_to_season(scen, 'historical_activity', hist_data)



# Modifying variable cost
yearly_to_season(scen, 'var_cost', fixed_data)




scen.commit(comment='introducing seasonality')
scen.set_as_default()


scen.solve()


scen.var('OBJ')['lvl']



from message_ix.reporting import Reporter
from message_ix.util.tutorial import prepare_plots

rep = Reporter.from_scenario(scen)
prepare_plots(rep)


rep.set_filters(t=["coal_ppl", "wind_ppl"])
rep.get("plot activity")


rep.get("plot capacity")


rep.set_filters(t=None, c=["light"])
rep.get("plot prices")


scen.var('ACT', {'year_act': 700})



mp.close_db()


