import pandas as pd
import ixmp
import message_ix

from message_ix.util import make_df



mp = ixmp.Platform(
    backend="jdbc",
    driver="hsqldb",
    url="jdbc:hsqldb:mem:temporary platform",
)


model = 'Westeros Electrified'

base = message_ix.Scenario(mp, model=model, scenario='baseline')
scen = base.clone(model, 'emission_bound','introducing an upper bound on emissions',
                  keep_solution=False)
scen.check_out()


year_df = scen.vintage_and_active_years()
vintage_years, act_years = year_df['year_vtg'], year_df['year_act']
model_horizon = scen.set('year')
country = 'Westeros'


# first we introduce the emission of CO2 and the emission category GHG
scen.add_set('emission', 'CO2')
scen.add_cat('emission', 'GHG', 'CO2')

# we now add CO2 emissions to the coal powerplant
base_emission_factor = {
    'node_loc': country,
    'year_vtg': vintage_years,
    'year_act': act_years,
    'mode': 'standard',
    'unit': 'tCO2/kWa',
}

# adding new units to the model library (needed only once)
mp.add_unit('tCO2/kWa')
mp.add_unit('MtCO2')

emission_factor = make_df(base_emission_factor, technology= 'coal_ppl', emission= 'CO2', value = 7.4)
scen.add_par('emission_factor', emission_factor)

scen.add_par('bound_emission', [country, 'GHG', 'all', 'cumulative'],
             value=500., unit='MtCO2')



scen.commit(comment='introducing emissions and setting an upper bound')
scen.set_as_default()



scen.solve()


scen.var('OBJ')['lvl']



from message_ix.reporting import Reporter
from message_ix.util.tutorial import prepare_plots

rep = Reporter.from_scenario(scen)
prepare_plots(rep)



rep.set_filters(t=["coal_ppl", "wind_ppl"])
rep.get("plot activity")


rep.get("plot capacity")


rep.set_filters(t=None, c=["light"])
rep.get("plot prices")



mp.close_db()








